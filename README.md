# Daily Mastodon Digest

> A Python script that aggregates recent popular posts from your Mastodon timeline

https://github.com/hodgesmr/mastodon_digest

## GitLab Page

This project contains a GitLab CI/CD Job that runs the above script and makes your "Mastodon Digest" available on a free [GitLab Page](https://about.gitlab.com/stages-devops-lifecycle/pages/).

How to set up:

1. Fork this repository
2. Create variables (Settings > CI/CD > Variables)
```shell
MASTODON_BASE_URL=https://example.com
MASTODON_USERNAME=example # without at sign and instance name
MASTODON_TOKEN=xyz # only read permissions required
```
3. Create a new schedule (CI/CD > Schedules > New schedule)
4. Run the CI job once manually (CI/CD > Schedules > Play)

After that, your "Mastodon Digest" is available on your GitLab page. You can find the URL under "Settings > Pages".
